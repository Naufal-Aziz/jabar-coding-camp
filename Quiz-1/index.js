// SOAL 1

function jumlah_kata(str) {
    var arr = str.split(' ');
    return arr.filter(kata => kata !== '').length;
  }

var kalimat_1= " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

console.log(jumlah_kata(kalimat_1))
console.log(jumlah_kata(kalimat_2))
console.log(jumlah_kata(kalimat_3))

// SOAL 2

function next_date(a, b, c){
    var besok = '';
    if(c % 4 == 0){
        switch(b){
            case 1: if(a+1 > 31){
                besok += 1 + ' Februari ' + c;
            }else{
                besok += a+1 + ' Januari ' + c;
            };
                break;
            case 2: if(a+1 > 29){
                besok += 1 + ' Maret ' + c;
            }else{
                besok += a+1 + ' Februari ' + c;
            };
                break;
            case 3: if(a+1 > 31){
                besok += 1 + ' April ' + c;
            }else{
                besok += a+1 + ' Maret ' + c;
            };
                break;
            case 4: if(a+1 > 30){
                besok += 1 + ' Mei ' + c;
            }else{
                besok += a+1 + ' April ' + c;
            };
                break;
            case 5: if(a+1 > 31){
                besok += 1 + ' Juni ' + c;
            }else{
                besok += a+1 + ' Mei ' + c;
            };
                break;
            case 6: if(a+1 > 30){
                besok += 1 + ' Juli ' + c;
            }else{
                besok += a+1 + ' Juni ' + c;
            };
                break;
            case 7: if(a+1 > 31){
                besok += 1 + ' Agustus ' + c;
            }else{
                besok += a+1 + ' Juli ' + c;
            };
                break;
            case 8: if(a+1 > 31){
                besok += 1 + ' September ' + c;
            }else{
                besok += a+1 + ' Agustus ' + c;
            };
            break;
            case 9: if(a+1 > 30){
                besok += 1 + ' Oktober ' + c;
            }else{
                besok += a+1 + ' September ' + c;
            };
                break;
            case 10: if(a+1 > 31){
                besok += 1 + ' November ' + c;
            }else{
                besok += a+1 + ' Oktober ' + c;
            };
                break;
            case 11: if(a+1 > 30){
                besok += 1 + ' Desember ' + c;
            }else{
                besok += a+1 + ' November ' + c;
            };
                break;
            case 12: if(a+1 > 31){
                besok += 1 + ' Januari ' + (c+1);
            }else{
                besok += a+1 + ' Desember ' + c;
            };
                break;
            default:
                break;
        }
    }else{
        switch(b){
            case 1: if(a+1 > 31){
                besok += 1 + ' Februari ' + c;
            }else{
                besok += a+1 + ' Januari ' + c;
            };
                break;
            case 2: if(a+1 > 28){
                besok += 1 + ' Maret ' + c;
            }else{
                besok += a+1 + ' Februari ' + c;
            };
                break;
            case 3: if(a+1 > 31){
                besok += 1 + ' April ' + c;
            }else{
                besok += a+1 + ' Maret ' + c;
            };
                break;
            case 4: if(a+1 > 30){
                besok += 1 + ' Mei ' + c;
            }else{
                besok += a+1 + ' April ' + c;
            };
                break;
            case 5: if(a+1 > 31){
                besok += 1 + ' Juni ' + c;
            }else{
                besok += a+1 + ' Mei ' + c;
            };
                break;
            case 6: if(a+1 > 30){
                besok += 1 + ' Juli ' + c;
            }else{
                besok += a+1 + ' Juni ' + c;
            };
                break;
            case 7: if(a+1 > 31){
                besok += 1 + ' Agustus ' + c;
            }else{
                besok += a+1 + ' Juli ' + c;
            };
                break;
            case 8: if(a+1 > 31){
                besok += 1 + ' September ' + c;
            }else{
                besok += a+1 + ' Agustus ' + c;
            };
            break;
            case 9: if(a+1 > 30){
                besok += 1 + ' Oktober ' + c;
            }else{
                besok += a+1 + ' September ' + c;
            };
                break;
            case 10: if(a+1 > 31){
                besok += 1 + ' November ' + c;
            }else{
                besok += a+1 + ' Oktober ' + c;
            };
                break;
            case 11: if(a+1 > 30){
                besok += 1 + ' Desember ' + c;
            }else{
                besok += a+1 + ' November ' + c;
            };
                break;
            case 12: if(a+1 > 31){
                besok += 1 + ' Januari ' + (c+1);
            }else{
                besok += a+1 + ' Desember ' + c;
            };
                break;
            default:
                break;
        }
    } return besok
}


var tanggal = 28;
var bulan = 2;
var tahun = 2048;

var x = next_date(tanggal, bulan, tahun)

console.log(x);