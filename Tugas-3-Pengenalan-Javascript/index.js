//TUGAS 3 MUHAMMAD NAUFAL AL AZIZ

//soal 1.1

var pertama = 'saya sangat senang hari ini';
var kedua = 'belajar javascript itu keren';

console.log(pertama.substring(0, 5).concat(pertama.substring(12, 19)).concat(kedua.substring(0, 8).concat(kedua.substring(8, 18).toUpperCase())));

// soal 1.2

var a = pertama.substring(0, 5).concat(pertama.substring(12, 19));
var b = kedua.substring(0, 8).concat(kedua.substring(8, 18).toUpperCase())

console.log(a.concat(b));

// soal 2.1

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var number1 = Number(kataPertama);
var number2 = Number(kataKedua);
var number3 = Number(kataKetiga);
var number4 = Number(kataKeempat);

console.log((number1 - number4) * (number2 + number3))

// soal 2.2
console.log(number1 * number2 + number3)

// soal 3

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);