// SOAL 1

var nilai = 98;

if(nilai >= 85){
    console.log('A')}
    else if(nilai >= 75 && nilai < 85){
        console.log('B')}
    else if(nilai >= 65 && nilai < 75){
        console.log('C')}
    else if(nilai >= 55 && nilai < 65){
        console.log('D')}
    else{
        console.log('E')
    }

// SOAL 2

var tanggal = 16;
var bulan = 11;
var tahun = 1995;

switch(bulan){
    case 1: console.log(tanggal +' Januari ' + tahun); break;
    case 2: console.log(tanggal +' Februari ' + tahun); break;
    case 3: console.log(tanggal +' Maret ' + tahun); break;
    case 4: console.log(tanggal +' April ' + tahun); break;
    case 5: console.log(tanggal +' Mei ' + tahun); break;
    case 6: console.log(tanggal +' Juni ' + tahun); break;
    case 7: console.log(tanggal +' Juli ' + tahun); break;
    case 8: console.log(tanggal +' Agustus ' + tahun); break;
    case 9: console.log(tanggal +' September ' + tahun); break;
    case 10: console.log(tanggal +' Oktober ' + tahun); break;
    case 11: console.log(tanggal +' November ' + tahun); break;
    case 12: console.log(tanggal +' Desember ' + tahun); break;
}

// SOAL 3

var n = 7;
var segitiga = '';
for(i =0; i < n; i++){
    segitiga += '#';
    console.log(segitiga);
}

// SOAL 4

var m = 10;
var line = '';
for(i = 1; i <= m; i++){
    if(i % 3 == 1){
        console.log(i + ' - I love Programming')
    }else if(i % 3 == 2){
        console.log(i + ' - I love Javascript')
    }else if(i % 3 == 0){
        console.log(i + ' - I love VueJS')
        line += '===';
        console.log(line);
    }
}