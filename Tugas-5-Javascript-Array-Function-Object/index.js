// SOAL 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort();

for(var i = 0; i < daftarHewan.length; i++){
    console.log(daftarHewan[i]);
}

// SOAL 2

function introduce() {
    return '"Nama saya ' + data.name + ', umur saya ' + data.age + ', alamat saya di ' + data.address + ', dan saya punya hobby yaitu ' + data.hobby + '"';
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };
 
var perkenalan = introduce(data);
console.log(perkenalan);

// console.log(introduce(data));

// SOAL 3

function hitung_huruf_vokal(nama) {
    var y = 0;
    for(var i = 0; i < nama.length; i++){
        if(nama[i] == 'a' || nama[i] == 'i' || nama[i] == 'u' || nama[i] == 'e' || nama[i] == 'o'){
            y++;
        }else if(nama[i] == 'A' || nama[i] == 'I' || nama[i] == 'U' || nama[i] == 'E' || nama[i] == 'O'){
            y++;
        }
    } return y;
}

var hitung_1 = hitung_huruf_vokal('Muhammad');

var hitung_2 = hitung_huruf_vokal('Iqbal');

console.log(hitung_1 , hitung_2);

// SOAL 4

function hitung(x) {
    return x * 2 - 2;
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));