// SOAL 1

const luasBangun = (panjang, lebar) => panjang * lebar;
const kelilingBangun = (panjang, lebar) => 2*panjang + 2*lebar;

console.log(luasBangun(4, 5));
console.log(kelilingBangun(4, 5));

// SOAL 2

const newFunction = (firstName, lastName) => {
    return{
        firstName,
        lastName,
        fullName(){
            console.log(`${firstName} ${lastName}`)
        }
    }
}
   
    //Driver Code 
    newFunction("William", "Imoh").fullName()

// SOAL 3
 
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const { firstName, lastName, address, hobby} = newObject;

console.log(firstName, lastName, address, hobby)

// SOAL 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]

    //Driver Code
    console.log(combined)

// SOAL 5

const planet = "earth" 
const view = "glass" 
const after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(after);